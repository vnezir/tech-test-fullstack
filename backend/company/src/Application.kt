package com.sedex.connect

import com.fasterxml.jackson.databind.SerializationFeature
import com.sedex.connect.model.CompanyRequest
import com.sedex.connect.repository.CompanyDB
import com.sedex.connect.repository.CompanyRepository
import com.sedex.connect.service.CompanyService
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    val db = CompanyDB()
    val repository = CompanyRepository(db)
    val service = CompanyService(repository)

    routing {
        get("/company") {
            call.respond(HttpStatusCode.OK, service.getAllCompanies())
        }
        get("/company/{id}") {
            call.parameters["id"]?.let { id ->
                service.getCompany(id)?.let {
                    call.respond(HttpStatusCode.OK, it)
                } ?: call.respond(HttpStatusCode.NotFound, "company not found")
            } ?: call.respond(HttpStatusCode.NotFound, "id missing")
        }
        post("/company") {
            val request = call.receive<CompanyRequest>()
            call.respond(HttpStatusCode.OK, mapOf("id" to service.createCompany(request)))
        }
    }
}


