package com.sedex.connect.repository

import com.sedex.connect.model.CompanyAddress
import com.sedex.connect.model.CompanyRequest
import com.sedex.connect.model.CompanyResponse
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.Instant
import org.joda.time.format.ISODateTimeFormat

class CompanyRepository(private val companyDB: CompanyDB) : Repository<CompanyResponse, CompanyRequest>{

    override fun persist(request: CompanyRequest): Long {
        return transaction(companyDB.instance) {
            try {
                CompanyTable.insert {
                    it[companyName] = request.companyName
                    it[companyType] = request.companyType
                    it[natureOfBusiness] = request.natureOfBusiness
                    it[incorporatedDate] = request.incorporatedDate
                    it[emailAddress] = request.emailAddress
                    it[phoneNumber] = request.phoneNumber
                    it[addressLine1] = request.address.addressLine1
                    it[addressLine2] = request.address.addressLine2
                    it[city] = request.address.city
                    it[state] = request.address.state
                    it[postalCode] = request.address.postalCode
                    it[countryCode] = request.address.countryCode
                    it[createdAt] = Instant.now().toDateTime()
                    it[updatedAt] = Instant.now().toDateTime()
                } get CompanyTable.id
            } catch (e: Exception) {
                rollback()
                throw e;
            }
        }
    }

    override fun get(id: Long): CompanyResponse? {
        val company = transaction(companyDB.instance) {
            CompanyTable.select { CompanyTable.id.eq(id) }.firstOrNull()
        }

        return company?.let {
            toCompanyModel(it)
        }
    }

    override fun get(): List<CompanyResponse> {
        val companies = transaction(companyDB.instance) {
            CompanyTable.selectAll().toList()
        }

        return companies.map { toCompanyModel(it) }
    }

    private fun toCompanyModel(it: ResultRow): CompanyResponse {
        val companyAddress = CompanyAddress(
            it[CompanyTable.addressLine1],
            it[CompanyTable.addressLine2],
            it[CompanyTable.city],
            it[CompanyTable.state],
            it[CompanyTable.postalCode],
            it[CompanyTable.countryCode]
        )

        return CompanyResponse(
            it[CompanyTable.id].toString(),
            it[CompanyTable.companyName],
            it[CompanyTable.companyType],
            it[CompanyTable.natureOfBusiness],
            it[CompanyTable.incorporatedDate],
            it[CompanyTable.emailAddress],
            it[CompanyTable.phoneNumber],
            companyAddress,
            it[CompanyTable.createdAt].toInstant().toString(ISODateTimeFormat.dateTime()),
            it[CompanyTable.updatedAt].toInstant().toString(ISODateTimeFormat.dateTime())
        )
    }

    object CompanyTable : Table("company") {
        val id = long("id").autoIncrement().primaryKey()
        val companyName = varchar("company_name", 50)
        val companyType = varchar("company_type", 50)
        val natureOfBusiness = varchar("nature_of_business", 50)
        val incorporatedDate = varchar("incorporated_date", 20)
        val emailAddress = varchar("email_address", 50)
        val phoneNumber = varchar("phone_number", 50)
        val addressLine1 = varchar("address_line1", 100)
        val addressLine2 = varchar("address_line2", 100)
        val city = varchar("city", 20)
        val state = varchar("start_time", 20)
        val postalCode = varchar("end_time", 20)
        val countryCode = varchar("row_count", 10)
        val createdAt = datetime("created_at")
        val updatedAt = datetime("updated_at")
    }

    override fun deleteAll() {
        transaction(companyDB.instance) {
            CompanyTable.deleteAll()
        }
    }

}