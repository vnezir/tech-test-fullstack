package com.sedex.connect.repository

import com.sedex.connect.repository.CompanyRepository.CompanyTable
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.SchemaUtils.create

class CompanyDB {

    val instance by lazy {
        val db = Database.connect(createConnectionPool())
        transaction {
            create(CompanyTable)
        }
        db
    }

    private fun createConnectionPool(): HikariDataSource {
        val config = HikariConfig()
        config.driverClassName = "org.h2.Driver"
        config.jdbcUrl = "jdbc:h2:mem:test"
        config.maximumPoolSize = 5
        config.validate()
        return HikariDataSource(config)
    }
}