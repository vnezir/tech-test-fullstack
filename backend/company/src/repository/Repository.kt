package com.sedex.connect.repository

interface Repository<out Model, in Req> {

    fun persist(request: Req): Long
    fun get(id: Long): Model?
    fun get(): List<Model>
    fun deleteAll()
}