package com.sedex.connect.service

import com.sedex.connect.model.CompanyList
import com.sedex.connect.model.CompanyRequest
import com.sedex.connect.model.CompanyResponse
import com.sedex.connect.repository.Repository

class CompanyService(private val repository: Repository<CompanyResponse, CompanyRequest>) {

    fun createCompany(request: CompanyRequest) : Long {
        return repository.persist(request)
    }

    fun getAllCompanies() : CompanyList {
        return CompanyList(repository.get())
    }

    fun getCompany(id: String): CompanyResponse? {
        return repository.get(id.toLong())
    }
}