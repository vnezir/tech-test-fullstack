package com.sedex.connect.model

data class CompanyRequest (
    val companyName: String,
    val companyType: String,
    val natureOfBusiness: String,
    val incorporatedDate: String,
    val emailAddress: String,
    val phoneNumber: String,
    val address: CompanyAddress
)