package com.sedex.connect.model

data class CompanyAddress(
    val addressLine1: String,
    val addressLine2: String,
    val city: String,
    val state: String,
    val postalCode: String,
    val countryCode: String
)
