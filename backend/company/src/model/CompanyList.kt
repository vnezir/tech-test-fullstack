package com.sedex.connect.model

data class CompanyList (val items: List<CompanyResponse>)