package repository

import com.sedex.connect.model.CompanyAddress
import com.sedex.connect.model.CompanyRequest
import com.sedex.connect.repository.CompanyDB
import com.sedex.connect.repository.CompanyRepository
import org.junit.Test
import kotlin.test.BeforeTest
import kotlin.test.assertEquals

class CompanyRepositoryTest {

    private val companyDB = CompanyDB()
    private val repository = CompanyRepository(companyDB)
    private val address = CompanyAddress("address1", "address2", "city", "state", "postcode", "code")
    private val request = CompanyRequest("name","companyType","nature","corp date","email address","12345", address)

    @BeforeTest
    fun deleteAll() {
        repository.deleteAll()
    }

    @Test
    fun testPersistAndGet() {
        repository.persist(request)
        val company = repository.get(1)!!

        assertEquals("name", company.companyName)
        assertEquals("companyType", company.companyType)
        assertEquals("nature", company.natureOfBusiness)
        assertEquals("corp date", company.incorporatedDate)
        assertEquals("email address", company.emailAddress)
        assertEquals("12345", company.phoneNumber)
        assertEquals("address1", company.address.addressLine1)
        assertEquals("address2", company.address.addressLine2)
        assertEquals("city", company.address.city)
        assertEquals("state", company.address.state)
        assertEquals("postcode", company.address.postalCode)
        assertEquals("code", company.address.countryCode)
    }

    @Test
    fun testPersistSeveralAndGetById() {
        repository.persist(request)
        repository.persist(request)
        repository.persist(request)
        val companies = repository.get()

        assertEquals(3, companies.size)
    }
}