package com.sedex.connect

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.sedex.connect.model.CompanyList
import com.sedex.connect.model.CompanyResponse
import com.sedex.connect.repository.CompanyDB
import com.sedex.connect.repository.CompanyRepository
import io.ktor.http.*
import io.ktor.server.testing.*
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class ApplicationTest {

    private val mapper = jacksonObjectMapper()

    @BeforeTest
    fun deleteRecords() {
        val repository = CompanyRepository(CompanyDB())
        repository.deleteAll()
    }

    @Test
    fun testPersistCompany() {
        withTestApplication({ module(testing = true) }) {
            handleRequest(HttpMethod.Post, "/company"){
                addHeader("Content-Type", "application/json")
                setBody(testPayload())
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    @Test
    fun testGetCompanyForId() {
        val mapper = jacksonObjectMapper()

        withTestApplication({ module(testing = true) }) {
            var id = ""
            handleRequest(HttpMethod.Post, "/company"){
                addHeader("Content-Type", "application/json")
                setBody(testPayload())
            }.apply {
                val contentMap: Map<String, String> = mapper.readValue(response.content!!)
                id = contentMap.getValue("id")
            }
            handleRequest(HttpMethod.Get, "/company/$id").apply {
                val company = response.content?.let {
                    mapper.readValue<CompanyResponse>(it)
                }
                assertEquals("Acme", company?.companyName)
                assertEquals("Retail", company?.companyType)
                assertEquals("Sports", company?.natureOfBusiness)
                assertEquals("01-01-2010", company?.incorporatedDate)
                assertEquals("info@acmeretail.com", company?.emailAddress)
                assertEquals("123456789", company?.phoneNumber)
                assertEquals("10 Cloverfield Lane", company?.address?.addressLine1)
                assertEquals("", company?.address?.addressLine2)
                assertEquals("New Orleans", company?.address?.city)
                assertEquals("Louisiana", company?.address?.state)
                assertEquals("", company?.address?.postalCode)
                assertEquals("US", company?.address?.countryCode)
                assertEquals(HttpStatusCode.OK, response.status())
            }
            handleRequest(HttpMethod.Get, "/company/9999").apply {
                assertEquals(HttpStatusCode.NotFound, response.status())
            }
        }
    }

    @Test
    fun testGetAllCompanies() {
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        withTestApplication({ module(testing = true) }) {
            handleRequest(HttpMethod.Get, "/company").apply {
                val companies = response.content?.let {
                    mapper.readValue<CompanyList>(it).items
                } ?: emptyList()

                assertEquals(0, companies.size)
                assertEquals(HttpStatusCode.OK, response.status())
            }
            handleRequest(HttpMethod.Post, "/company"){
                addHeader("Content-Type", "application/json")
                setBody(testPayload())
            }
            handleRequest(HttpMethod.Post, "/company"){
                addHeader("Content-Type", "application/json")
                setBody(testPayload())
            }
            handleRequest(HttpMethod.Post, "/company"){
                addHeader("Content-Type", "application/json")
                setBody(testPayload())
            }
            handleRequest(HttpMethod.Get, "/company").apply {
                val companies = response.content?.let {
                    mapper.readValue<CompanyList>(it).items
                } ?: emptyList()

                assertEquals(3, companies.size)
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    private fun testPayload(
        companyName: String = "Acme",
        companyType: String = "Retail",
        natureOfBusiness: String = "Sports",
        incorporatedDate: String = "01-01-2010",
        emailAddress: String = "info@acmeretail.com",
        phoneNumber: String = "123456789",
        addressLine1: String = "10 Cloverfield Lane",
        addressLine2: String = "",
        city: String = "New Orleans",
        state: String = "Louisiana",
        postalCode: String = "",
        countryCode: String = "US"
    ): String {
        return """
            {
                "companyName":"$companyName",
                "companyType":"$companyType",
                "natureOfBusiness":"$natureOfBusiness",
                "incorporatedDate":"$incorporatedDate",
                "emailAddress":"$emailAddress",
                "phoneNumber":"$phoneNumber",
                "address":{
                    "addressLine1":"$addressLine1",
                    "addressLine2":"$addressLine2",
                    "city":"$city",
                    "state":"$state",
                    "postalCode":"$postalCode",
                    "countryCode":"$countryCode"
                }
            }
        """.trimIndent()
    }
}
