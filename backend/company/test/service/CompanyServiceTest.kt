package service

import com.sedex.connect.model.CompanyAddress
import com.sedex.connect.model.CompanyRequest
import com.sedex.connect.repository.CompanyRepository
import com.sedex.connect.service.CompanyService
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class CompanyServiceTest {

    private val repository = mock(CompanyRepository::class.java)
    private val service = CompanyService(repository)

    @Test
    fun testCreateCompany() {
        val address = CompanyAddress("address1", "address2", "city", "state", "postcode", "code")
        val request = CompanyRequest("name","companyType","nature","corp date","email address","12345", address)
        service.createCompany(request)

        verify(repository).persist(request)
    }

    @Test
    fun testGetAllCompanies() {
        service.getAllCompanies()

        verify(repository).get()
    }

    @Test
    fun testGetCompany() {
        service.getCompany("1")

        verify(repository).get(1L)
    }

}