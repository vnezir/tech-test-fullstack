# Company API

Additional libraries used:

  1. H2 - in memory database for persistence, granted this is volatile and ideally I would have either a relational db or object db based on future requirements.   
  1. Exposed - Kotlin SQL framework for interacting with H2
  1. Hikari CP - Lightweight JDBC connection pooling library 
  1. Mockito - Mocking framework used in testing briefly

## How to test

* Run the main Application in Intellij
* Use the provided sample endpoints as provided in the intellij http clients /api/company.http, select "run as local".
Feel free to amend payload as you wish. 

## If I had more time and less family distractions...

* Use a persistent storage solution be it relational or object db based on requirements.
* I would have liked to have used Test Containers for testing but time constraints and sorting out my docker environment which is set up for my current workplace would not have made this feasible.
* Make some fields in the models optional, I realised this once I had completed the test and the changes required I think would have exceeded the time limit. 
* Dockerise the application so that we can have a self container application container which we publish to a docker registry and potentially make available in a clustered container orchestration system i.e. Kubernetes 